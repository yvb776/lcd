﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using USB_IO_Family32;
using CoreAudioApi;

namespace LCD
{
	public partial class Form1 : Form
	{
		ioCtl io = new ioCtl();
		private Thread th;
		private TcpListener server;
		string str;
		int i = 0; //カウンタ
		private MMDevice device;
		int volume; //現在のマスタ音量

		bool busyflag = false; //実験的実装！
		int ScrollCnt = 0;
		bool chkScrollModeSave = false;

		int prevLVol = 101, prevRVol = 101, prevMVol = 101;//保持用

		PerformanceCounter pc = new System.Diagnostics.PerformanceCounter();
		PerformanceCounter pc_mem = new System.Diagnostics.PerformanceCounter();

		public Form1()
		{
			InitializeComponent();
			this.Disposed += Form1_Disposed;
			MMDeviceEnumerator DevEnum = new MMDeviceEnumerator();
			device = DevEnum.GetDefaultAudioEndpoint(EDataFlow.eRender, ERole.eMultimedia);
			volume = (int)(device.AudioEndpointVolume.MasterVolumeLevelScalar * 100);
			device.AudioEndpointVolume.OnVolumeNotification += new AudioEndpointVolumeNotificationDelegate(AudioEndpointVolume_OnVolumeNotification);
			//timerAudioVol.Enabled = true;
			lblScrollCnt.Text = ScrollCnt.ToString();
		}

		void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data)
		{
			if (this.InvokeRequired)
			{
				object[] Params = new object[1];
				Params[0] = data;
				this.Invoke(new AudioEndpointVolumeNotificationDelegate(AudioEndpointVolume_OnVolumeNotification), Params);
			}
			else
			{
				volume = (int)(data.MasterVolume * 100);
			}
		}



		void Form1_Disposed(object sender, EventArgs e)
		{
			if (rbAutoTime.Checked)
			{
				bgAutoTime.CancelAsync();
				System.Threading.Thread.Sleep(1010);
			}

			if (rbAudio.Checked)
			{
				rbAudio.Checked = false;
				System.Threading.Thread.Sleep(300);
			}

			outFunc("00000001");
			System.Threading.Thread.Sleep(2);
			outStr("LCD.exe is Not", 0);
			outLocate(0, 1);
			outStr("Running.", 3);
			io.closeDevice();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			// openDeviceにより接続デバイスを検索する
			if (io.openDevice() <= 0)
			{
				MessageBox.Show("USB-IO2.0が見つかりません", "Error!");
				System.Environment.Exit(0);
			}
			// LCDの初期化関数を呼び出す
			initLcd();
			cnt0.Text = txtAll0.Text.Length.ToString();
			cnt1.Text = txtAll1.Text.Length.ToString();

			pc.CategoryName = "Processor";
			pc.CounterName = "% Processor Time";
			pc.InstanceName = "_Total";

			pc_mem.CategoryName = "Memory";
			pc_mem.CounterName = "Available MBytes";

			outFunc("01001000");    //Set CGRAM Adress 01 001 000 0x01

			setFont("10001");    //MB
			setFont("11011");
			setFont("10101");
			setFont("00000");
			setFont("11110");
			setFont("11111");
			setFont("10001");
			setFont("11110");

			outFunc("01010000"); //0x02

			setFont("00001");   //Volumeアイコン
			setFont("00011");
			setFont("10111");
			setFont("11111");
			setFont("10111");
			setFont("00011");
			setFont("00001");
			setFont("00000");
		}


		// LCDの初期化関数
		private void initLcd()
		{
			// 設定データを送信する関数
			outFunc("00110000");    // Function
			System.Threading.Thread.Sleep(5);
			outFunc("00110000");    // Function
			System.Threading.Thread.Sleep(5);
			outFunc("00110000");    // Function
			outFunc("00111100");    // Function Set
			outFunc("00001100");    // Display
			outFunc("00000001");    // Clear
			System.Threading.Thread.Sleep(5);
			outFunc("00000110");    // Entry Mode
			outFunc("00010100");    // Cursor
			outFunc("00000010");    // Cursor Home
			System.Threading.Thread.Sleep(5);

			outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000
			//↑01000000
		}

		// 設定データを送信する関数
		private void outFunc(string code2)
		{
			while (busyflag) Thread.Sleep(100); //実験的実装！
			busyflag = true;

			ioCtl.ST_CTL_OUTPUT[] stOut = new ioCtl.ST_CTL_OUTPUT[3];
			ioCtl.ST_CTL_INPUT stInDmy = new ioCtl.ST_CTL_INPUT();
			stOut[0].Port = 1;  // ポート1
			stOut[0].Data = Convert.ToByte(code2, 2);   // 2進数をセット

			stOut[1].Port = 2;  // ポート2
			stOut[1].Data = Convert.ToByte("1010", 2);  // J2 3:E 2:R/W 1:LED 0:RS

			io.ctlInOut(ref stInDmy, stOut);

			stOut[1].Data = Convert.ToByte("0000", 2);
			io.ctlInOut(ref stInDmy, stOut);

			busyflag = false;
		}
		// CGRAMにユーザ定義文字を送信するための関数
		private void setFont(string code2)
		{
			while (busyflag) Thread.Sleep(100); //実験的実装！
			busyflag = true;

			ioCtl.ST_CTL_OUTPUT[] stOut = new ioCtl.ST_CTL_OUTPUT[3];
			ioCtl.ST_CTL_INPUT stInDmy = new ioCtl.ST_CTL_INPUT();
			stOut[0].Port = 1;  // ポート1
			stOut[0].Data = Convert.ToByte(code2, 2);   // 2進数をセット

			stOut[1].Port = 2;  // ポート2
			stOut[1].Data = Convert.ToByte("1011", 2);  // J2 3:E 2:R/W 1:LED 0:RS

			io.ctlInOut(ref stInDmy, stOut);

			stOut[1].Data = Convert.ToByte("0000", 2);
			io.ctlInOut(ref stInDmy, stOut);

			busyflag = false;
		}

		// 表示位置を送信する関数
		private void outLocate(byte x, byte y)
		{
			while (busyflag) Thread.Sleep(100); //実験的実装！
			busyflag = true;

			ioCtl.ST_CTL_OUTPUT[] stOut = new ioCtl.ST_CTL_OUTPUT[3];
			ioCtl.ST_CTL_INPUT stInDmy = default(ioCtl.ST_CTL_INPUT);
			// DDRAMアドレスセットのコマンドを作成する
			stOut[0].Port = 1;
			stOut[0].Data = (byte)(Convert.ToByte("10000000", 2) | (y * 0x40) | x);

			stOut[1].Port = 2;
			stOut[1].Data = Convert.ToByte("1010", 2);
			io.ctlInOut(ref stInDmy, stOut);

			stOut[1].Data = Convert.ToByte("0000", 2);
			io.ctlInOut(ref stInDmy, stOut);

			busyflag = false;
		}

		private void outChar(string charCode)
		{
			while (busyflag) Thread.Sleep(100); //実験的実装！
			busyflag = true;

			byte[] bytesData = new byte[8];
			bytesData = System.Text.Encoding.GetEncoding(932).GetBytes(charCode);
			ioCtl.ST_CTL_OUTPUT[] stOut = new ioCtl.ST_CTL_OUTPUT[3];
			ioCtl.ST_CTL_INPUT stInDmy = default(ioCtl.ST_CTL_INPUT);
			stOut[0].Port = 1;
			// 文字データをASCIIコードに変換する
			stOut[0].Data = bytesData[0];
			stOut[1].Port = 2;
			stOut[1].Data = Convert.ToByte("1011", 2);
			io.ctlInOut(ref stInDmy, stOut);

			stOut[1].Data = Convert.ToByte("0001", 2);
			io.ctlInOut(ref stInDmy, stOut);

			busyflag = false;
		}

		private void outCharCode(byte charCode)
		{
			while (busyflag) Thread.Sleep(100); //実験的実装！
			busyflag = true;

			//引数にASCIIコード そのまま出力
			ioCtl.ST_CTL_OUTPUT[] stOut = new ioCtl.ST_CTL_OUTPUT[3];
			ioCtl.ST_CTL_INPUT stInDmy = default(ioCtl.ST_CTL_INPUT);
			stOut[0].Port = 1;
			// ASCIIコードを出力
			stOut[0].Data = charCode;
			stOut[1].Port = 2;
			stOut[1].Data = Convert.ToByte("1011", 2);
			io.ctlInOut(ref stInDmy, stOut);

			stOut[1].Data = Convert.ToByte("0001", 2);
			io.ctlInOut(ref stInDmy, stOut);

			busyflag = false;
		}

		private void btnAll_Click(object sender, EventArgs e)
		{
			int i = 0;
			outFunc("00000001");
			// LCDへの表示位置（桁と行）を設定する
			outLocate(0, 0);
			// 画面の文字列を1文字ずつ送信する関数を呼び出す
			for (i = 0; i <= txtAll0.Text.Length - 1; i++)
			{
				outChar(txtAll0.Text.Substring(i, 1));
			}
			outLocate(0, 1);
			for (i = 0; i <= txtAll1.Text.Length - 1; i++)
			{
				outChar(txtAll1.Text.Substring(i, 1));
			}
		}

		private void bgAutoTime_DoWork(object sender, DoWorkEventArgs e)
		{
			int bgAutoTimeArg = Convert.ToInt32(e.Argument);
			BackgroundWorker autotime = (BackgroundWorker)sender;
			DateTime dtNow = DateTime.Now;
			string strdtNow;
			string strdtNow2;
			int Second = dtNow.Second;
			int Weeknum = 7; //初期は無効な曜日
			int i;

			for (; ; )
			{
				if (autotime.CancellationPending)
				{
					e.Cancel = true;
					break;
				}

				System.Threading.Thread.Sleep(60);
				if (Weeknum != (int)DateTime.Now.DayOfWeek)//日付変わったら実行される
				{
					Weeknum = (int)DateTime.Now.DayOfWeek;
					switch (Weeknum)
					{
						case 0:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("01111");    //日
							setFont("01001");
							setFont("01001");
							setFont("01111");
							setFont("01001");
							setFont("01001");
							setFont("01111");
							setFont("00000");
							break;
						case 1:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("01111");    //月
							setFont("01001");
							setFont("01111");
							setFont("01001");
							setFont("01111");
							setFont("01001");
							setFont("10001");
							setFont("00000");
							break;
						case 2:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("00100");    //火
							setFont("10101");
							setFont("10101");
							setFont("00100");
							setFont("01010");
							setFont("01010");
							setFont("10001");
							setFont("00000");
							break;
						case 3:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("00100");    //水
							setFont("00101");
							setFont("11110");
							setFont("00110");
							setFont("01101");
							setFont("10100");
							setFont("00100");
							setFont("00000");
							break;
						case 4:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("00100");    //木
							setFont("00100");
							setFont("11111");
							setFont("00100");
							setFont("01110");
							setFont("10101");
							setFont("00100");
							setFont("00000");
							break;
						case 5:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("01110");    //金
							setFont("10001");
							setFont("01110");
							setFont("11111");
							setFont("00100");
							setFont("10101");
							setFont("11111");
							setFont("00000");
							break;
						case 6:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("00100");    //土
							setFont("00100");
							setFont("11111");
							setFont("00100");
							setFont("00100");
							setFont("00100");
							setFont("11111");
							setFont("00000");
							break;
						default:
							outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000

							setFont("11111");    //Err
							setFont("10001");
							setFont("10001");
							setFont("10001");
							setFont("10001");
							setFont("10001");
							setFont("11111");
							setFont("00000");
							break;
					}
					System.Threading.Thread.Sleep(50);
				}

				//1秒ごとに実行
				if (Second != DateTime.Now.Second)
				{
					dtNow = DateTime.Now;
					//strdtNow = dtNow.ToString(" MM/dd HH:mm:ss");
					strdtNow = dtNow.ToString("MM/dd ");
					strdtNow2 = dtNow.ToString(" HH:mm:ss");
					Second = dtNow.Second;


					outLocate(0, 0);
					for (i = 0; i <= strdtNow.Length - 1; i++)
					{
						outChar(strdtNow.Substring(i, 1));
					}
					outCharCode(0x00); //曜日
					for (i = 0; i <= strdtNow2.Length - 1; i++)
					{
						outChar(strdtNow2.Substring(i, 1));
					}
					outLocate(0, 1);
					outStr(string.Format("{0:000}% ", pc.NextValue()), 3);
					outCharCode(0x02); //Volumeアイコン
					outStr(string.Format("{0,3}%", Convert.ToString(volume)), 3);
					outStr(string.Format("{0,5}", pc_mem.NextValue()), 3);


					outCharCode(0x01); //MB
				}
			}
		}


		private void bgAutoTime_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
		{
			if (e.Cancelled)
			{
				outFunc("00000001");
				if (!rbNetText.Checked)
				{
					btnAll.Enabled = true;
					btnAllClear.Enabled = true;
					btnLCDinit.Enabled = true;
					btnLCDTest.Enabled = true;
					btnLCDTest2.Enabled = true;
					//btnLeftShift.Enabled = true;
					//btnRightShift.Enabled = true;
					//chkBoxAutoScroll.Enabled = true;
					//chkBoxReverse.Enabled = true;
				}
				this.Enabled = true;
			}
		}

		private void AllClearbtn_Click(object sender, EventArgs e)
		{
			outFunc("00000001");
		}

		private void rbAutoTime_CheckedChanged(object sender, EventArgs e)
		{
			ScrollReset();
			if (rbAutoTime.Checked)
			{
				outFunc("00000001");
				bgAutoTime.RunWorkerAsync();
				btnAll.Enabled = false;
				btnAllClear.Enabled = false;
				btnLCDinit.Enabled = false;
				btnLCDTest.Enabled = false;
				btnLCDTest2.Enabled = false;
				//btnLeftShift.Enabled = false;
				//btnRightShift.Enabled = false;
				//chkBoxAutoScroll.Enabled = false;
				//chkBoxReverse.Enabled = false;
				lblNetStatus.Text = "NetWork OFF";
			}
			else
			{
				bgAutoTime.CancelAsync();
				this.Enabled = false;
				System.Threading.Thread.Sleep(100);
			}
		}

		private void rbNetText_CheckedChanged(object sender, EventArgs e)
		{
			ScrollReset();
			if (rbNetText.Checked)
			{
				outFunc("00000001");
				btnAll.Enabled = false;
				btnAllClear.Enabled = false;
				btnLCDinit.Enabled = false;
				btnLCDTest.Enabled = false;
				btnLCDTest2.Enabled = false;
				//btnLeftShift.Enabled = false;
				//btnRightShift.Enabled = false;
				//chkBoxAutoScroll.Enabled = false;
				//chkBoxReverse.Enabled = false;
				th = new Thread(DataListener);
				th.Start();
				lblNetStatus.Text = "NetWork ON";
			}
			else
			{
				server.Stop();
			}
		}

		private void rbAutoOff_CheckedChanged(object sender, EventArgs e)
		{
			ScrollReset();
			if (rbAutoOff.Checked)
			{
				outFunc("00000001");
				btnAll.Enabled = true;
				btnAllClear.Enabled = true;
				btnLCDinit.Enabled = true;
				btnLCDTest.Enabled = true;
				btnLCDTest2.Enabled = true;
				//btnLeftShift.Enabled = true;
				//btnRightShift.Enabled = true;
				//chkBoxAutoScroll.Enabled = true;
				//chkBoxReverse.Enabled = true;
				lblNetStatus.Text = "NetWork OFF";
			}
		}

		private void txtAll0_TextChanged(object sender, EventArgs e)
		{
			cnt0.Text = txtAll0.TextLength.ToString();
		}

		private void txtAll1_TextChanged(object sender, EventArgs e)
		{
			cnt1.Text = txtAll1.TextLength.ToString();
		}

		private void btnL0Clear_Click(object sender, EventArgs e)
		{
			txtAll0.Text = "";
		}

		private void btnL1Clear_Click(object sender, EventArgs e)
		{
			txtAll1.Text = "";
		}

		private void btnLCDTest_Click(object sender, EventArgs e)
		{
			//outFunc("00001000");//OFF
			//outFunc("00001100");//ON
			//outFunc("00001110");//カーソルON
			//outFunc("00001101");//ブリンクON
			//outFunc("00001111");//カーソル、ブリンクON

			outFunc("01000000");    //Set CGRAM Address 01 000:0x00(CGRAM(0)) 000
			//↑01000000
			/*

			setFont("00100");    //˙
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			*/

			outFunc("01001000");    //Set CGRAM Adress 01 001 000 0x01

			setFont("10001");    //MB
			setFont("11011");
			setFont("10101");
			setFont("11110");
			setFont("11111");
			setFont("10001");
			setFont("11110");
			setFont("00000");

			outFunc("01010000");    //01 010 000 0x02

			setFont("00000");   //◉
			setFont("00000");
			setFont("01110");
			setFont("11111");
			setFont("10111");
			setFont("10101");
			setFont("01110");
			setFont("00000");

			outFunc("01011000");    //01 011 000 0x03

			setFont("00100");   //´
			setFont("01000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00000");

			outFunc("01100000");    //01 100 000 0x04

			setFont("00000");   //◞
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("00010");
			setFont("01100");
			setFont("00000");
			setFont("00000");

			outFunc("01101000");    //01 101 000 0x05

			setFont("00000");   //◟
			setFont("00000");
			setFont("00000");
			setFont("00000");
			setFont("01000");
			setFont("00110");
			setFont("00000");
			setFont("00000");

			outFunc("01110000");    //01 110 000 0x06

			setFont("00000");   //౪
			setFont("00000");
			setFont("00000");
			setFont("01010");
			setFont("11111");
			setFont("01010");
			setFont("00100");
			setFont("00000");

			outFunc("01111000");    //01 111 000 0x07

			setFont("00000");   //ヽ
			setFont("00000");
			setFont("01000");
			setFont("01100");
			setFont("00110");
			setFont("00011");
			setFont("00000");
			setFont("00000");

		}

		//outStr_Net関数は先頭1文字をコマンドとし、2文字目以降をLCDに表示する
		private void outStr_Net(string s)
		{
			int i;

			//1文字目が0なら1行目、1なら二行目。2,3は画面の初期化無し
			if (s.StartsWith("0"))
			{
				outFunc("00000001");
				outLocate(0, 0);
				for (i = 1; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else if (s.StartsWith("1"))
			{
				outFunc("00000001");
				outLocate(0, 1);
				for (i = 1; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else if (s.StartsWith("2"))
			{
				//outLocate(0, 0);
				for (i = 1; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else if (s.StartsWith("3"))
			{
				//outLocate(0, 1);
				for (i = 1; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else outStr("ﾌｾｲﾅﾃﾞｰﾀ", 0);
		}

		private void outStr(string s, int y)
		{
			int i = 0;

			//第二引数が0の場合は1行目に、1なら2行目に表示、2,3は画面の初期化無し
			if (y == 0)
			{
				outFunc("00000001");
				outLocate(0, 0);
				for (i = 0; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else if (y == 1)
			{
				outFunc("00000001");
				outLocate(0, 1);
				for (i = 0; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else if (y == 2)
			{
				//outLocate(0, 0);
				for (i = 0; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
			else if (y == 3)
			{
				//outLocate(0, 1);
				for (i = 0; i <= s.Length - 1; i++)
				{
					outChar(s.Substring(i, 1));
				}
			}
		}

		private void DataListener()
		{
			try
			{
				server = new TcpListener(IPAddress.Parse("127.0.0.1"), 25914);

				server.Start();
				// lblNetStatus.Text = "Listening Port:25914";
				outStr("Listening...", 0);
				outLocate(0, 1);
				outStr("Port:25914", 3);

				while (true)
				{
					if (!rbNetText.Checked)
						break;

					TcpClient client = server.AcceptTcpClient();
					//   lblNetStatus.Text = "Connecting...";

					NetworkStream ns = client.GetStream();
					//   lblNetStatus.Text = "Wait...";

					byte[] data = new byte[100];
					int len = ns.Read(data, 0, data.Length);
					str = System.Text.Encoding.UTF8.GetString(data, 0, len);

					outStr_Net(str);

					client.Close();
					//    lblNetStatus.Text = "Closed";
				}
			}
			catch
			{

			}
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (rbNetText.Checked)
				e.Cancel = true;
			else
				notifyIcon.Visible = false;
		}

		private void btnLCDinit_Click(object sender, EventArgs e)
		{
			// 設定データを送信する関数を呼び出す
			outFunc("00110000");
			// Function
			// Sleepメソッドを呼び出して待機する
			System.Threading.Thread.Sleep(5);
			outFunc("00110000");
			// Function
			System.Threading.Thread.Sleep(5);
			outFunc("00110000");
			// Function
			//System.Threading.Thread.Sleep(5)
			if (rbFont5x10.Checked)
			{
				if (rb2Line.Checked)
					outFunc("00111100");
				else
					outFunc("00110100");
			}
			else
			{
				if (rb2Line.Checked)
					outFunc("00111000");
				else
					outFunc("00110000");
			}
			// Function Set
			outFunc("00001100");
			// Display
			outFunc("00000001");
			// Clear
			System.Threading.Thread.Sleep(5);
			outFunc("00000110");
			// Entry Mode
			outFunc("00010100");
			// Cursor
			outFunc("00000010");
			// Cursor Home
			System.Threading.Thread.Sleep(5);
		}

		private void rb1Line_CheckedChanged(object sender, EventArgs e)
		{
			if (rb1Line.Checked)
				rbFont5x10.Enabled = true;
			else
				rbFont5x10.Enabled = false;

		}

		private void Form1_ClientSizeChanged(object sender, EventArgs e)
		{
			if (this.WindowState == System.Windows.Forms.FormWindowState.Minimized)
			{
				this.Hide();
				notifyIcon.Visible = true;
			}
		}

		private void notifyIcon_DoubleClick(object sender, EventArgs e)
		{
			this.Visible = true;
			if (this.WindowState == FormWindowState.Minimized)
				this.WindowState = FormWindowState.Normal;
			notifyIcon.Visible = false;
			this.Activate();
		}

		private void btnLCDTest2_Click(object sender, EventArgs e)
		{
			outLocate(0, 0);
			outCharCode(0x07);
			outChar("(");

			//0x03 0x02 0x04 0x06 0x05
			outCharCode(0x03);
			outCharCode(0x02);
			outCharCode(0x04);
			outCharCode(0x06);
			outCharCode(0x05);
			outCharCode(0x02);
			outChar(")");
			outChar("ﾉ");

			outLocate(16, 1);

			outCharCode(0x07);
			outChar("(");

			//0x03 0x02 0x04 0x06 0x05
			outCharCode(0x03);
			outCharCode(0x02);
			outCharCode(0x04);
			outCharCode(0x06);
			outCharCode(0x05);
			outCharCode(0x02);
			outChar(")");
			outChar("ﾉ");

			outLocate(25, 0);
			outCharCode(0x07);
			outChar("(");

			//0x03 0x02 0x04 0x06 0x05
			outCharCode(0x03);
			outCharCode(0x02);
			outCharCode(0x04);
			outCharCode(0x06);
			outCharCode(0x05);
			outCharCode(0x02);
			outChar(")");
			outChar("ﾉ");
		}

		private void btnLeftShift_Click(object sender, EventArgs e)
		{
			ScrollLeft();
		}

		private void btnRightShift_Click(object sender, EventArgs e)
		{
			ScrollRight();
		}

		private void chkBoxAutoScroll_CheckedChanged(object sender, EventArgs e)
		{
			ScrollReset();
			if (chkBoxAutoScroll.Checked)
			{
				i = 0;
				chkIIDX.Enabled = false;
				if (chkIIDX.Checked)
					timerAutoScroll.Interval = 250;
				else
					timerAutoScroll.Interval = 200;
				timerAutoScroll.Start();
			}
			else
			{
				timerAutoScroll.Stop();
				chkIIDX.Enabled = true;
			}
		}

		private void timerAutoScroll_Tick(object sender, EventArgs e)
		{
			i++;
			if(chkIIDX.Checked)
				ScrollLeft();
			else
			{
				if (!chkBoxReverse.Checked)
					ScrollRight();
				else
					ScrollLeft();
			}
		}

		private void timerAudioVol_Tick(object sender, EventArgs e)
		{
			int MasterVol = (int)(device.AudioMeterInformation.MasterPeakValue * 100);
			int leftVol = (int)(device.AudioMeterInformation.PeakValues[0] * 100);
			int rightVol = (int)(device.AudioMeterInformation.PeakValues[1] * 100);

			labelMasterVol.Text = MasterVol.ToString();
			labelLeftVol.Text = leftVol.ToString();
			labelRightVol.Text = rightVol.ToString();

			//TODO:ToString().Length用いて短縮

			if (rbAudio.Checked)
			{

				if (MasterVol == 100) //現在のMが100%
				{
					if (MasterVol != prevMVol)
					{
						outLocate(9, 1);
						outStr(string.Format("{0,2}", MasterVol), 3);
					}

					if (rightVol == 100 && leftVol == 100) //L,Rどちらも100%
					{
						if (rightVol != prevRVol)
						{
							outLocate(12, 0);
							outStr(string.Format("{0,3}", rightVol), 2);
						}
						if (leftVol != prevLVol)
						{
							outLocate(2, 0);
							outStr(string.Format("{0,3}", leftVol), 2);
						}
					}
					else if (leftVol == 100) //Lだけ100%
					{
						if (leftVol != prevLVol)
						{
							outLocate(2, 0);
							outStr(string.Format("{0,3}", leftVol), 2);
						}

						if (rightVol != prevRVol)
						{
							if (prevRVol == 100)
							{
								outLocate(12, 0);
								outStr(string.Format("{0,3}", rightVol), 2);
							}
							else
							{
								outLocate(13, 0);
								outStr(string.Format("{0,2}", rightVol), 2);
							}
						}
					}
					else if (rightVol == 100) //Rだけ100%
					{
						if (rightVol != prevRVol)
						{
							outLocate(12, 0);
							outStr(string.Format("{0,3}", rightVol), 2);
						}

						if (leftVol != prevLVol)
						{
							if (prevLVol == 100)
							{
								outLocate(2, 0);
								outStr(string.Format("{0,3}", leftVol), 2);
							}
							else
							{
								outLocate(3, 0);
								outStr(string.Format("{0,2}", leftVol), 2);
							}
						}
					}
				}

				else if (MasterVol != 100 && prevMVol == 100) //前のMの更新が100%で、現在は違う
				{
					outLocate(9, 1);
					outStr(string.Format("{0,3}", MasterVol), 3);

					if (leftVol != prevLVol)
					{
						if (prevLVol == 100)
						{
							outLocate(2, 0);
							outStr(string.Format("{0,3}", leftVol), 2);
						}
						else
						{
							outLocate(3, 0);
							outStr(string.Format("{0,2}", leftVol), 2);
						}
					}
					if (rightVol != prevRVol)
					{
						if (prevRVol == 100)
						{
							outLocate(12, 0);
							outStr(string.Format("{0,3}", rightVol), 2);
						}
						else
						{
							outLocate(13, 0);
							outStr(string.Format("{0,2}", rightVol), 2);
						}
					}
				}

				else
				{
					if (leftVol != prevLVol)
					{
						outLocate(3, 0);
						outStr(string.Format("{0,2}", leftVol), 2);
					}
					if (rightVol != prevRVol)
					{
						outLocate(13, 0);
						outStr(string.Format("{0,2}", rightVol), 2);
					}
					if (MasterVol != prevMVol)
					{
						outLocate(10, 1);
						outStr(string.Format("{0,2}", MasterVol), 3);
					}
				}
			}

			prevLVol = leftVol;
			prevRVol = rightVol;
			prevMVol = MasterVol;

		}

		private void rbAudio_CheckedChanged(object sender, EventArgs e)
		{
			if (rbAudio.Checked)
			{
				outFunc("00000001"); //LCD初期化

				timerAudioVol.Enabled = true;

				btnAll.Enabled = false;
				btnAllClear.Enabled = false;
				btnLCDinit.Enabled = false;
				btnLCDTest.Enabled = false;
				btnLCDTest2.Enabled = false;
				//btnLeftShift.Enabled = false;
				//btnRightShift.Enabled = false;
				//chkBoxAutoScroll.Enabled = false;
				//chkBoxReverse.Enabled = false;
				lblNetStatus.Text = "NetWork OFF";
				outStr("L:", 2);
				outLocate(5, 0);
				outChar("%");

				outLocate(10, 0);
				outStr("R:", 2);
				outLocate(15, 0);
				outChar("%");

				outLocate(2, 1);
				outStr("Master:", 3);
				outLocate(12, 1);
				outChar("%");

			}
			else
			{
				timerAudioVol.Enabled = false;
				prevLVol = 101;
				prevMVol = 101;
				prevRVol = 101;
			}
		}

		private void btnReset_Click(object sender, EventArgs e)
		{
			ScrollReset();
		}

		void ScrollReset()
		{
			while (ScrollCnt > 0 && ScrollCnt < 19)
				ScrollLeft();

			while (ScrollCnt >= 19)
				ScrollRight();
		}

		void ScrollLeft()
		{
			outFunc("00011000");
			ScrollCnt--;
			if (ScrollCnt < 0)
				ScrollCnt = 39;
			lblScrollCnt.Text = ScrollCnt.ToString();
		}

		void ScrollRight()
		{
			outFunc("00011100");
			ScrollCnt++;
			if (ScrollCnt > 39)
				ScrollCnt = 0;
			lblScrollCnt.Text = ScrollCnt.ToString();
		}

		private void chkIIDX_CheckedChanged(object sender, EventArgs e)
		{
			if(chkIIDX.Checked)
			{
				chkScrollModeSave = chkBoxReverse.Checked;
				chkBoxReverse.Checked = true;
				chkBoxReverse.Enabled = false;
			}
			else
			{
				chkBoxReverse.Enabled = true;
				chkBoxReverse.Checked = chkScrollModeSave;
			}
		}

	}
}
