﻿namespace LCD
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.btnAllClear = new System.Windows.Forms.Button();
			this.btnAll = new System.Windows.Forms.Button();
			this.txtAll1 = new System.Windows.Forms.TextBox();
			this.txtAll0 = new System.Windows.Forms.TextBox();
			this.bgAutoTime = new System.ComponentModel.BackgroundWorker();
			this.gboxAutoMode = new System.Windows.Forms.GroupBox();
			this.rbAudio = new System.Windows.Forms.RadioButton();
			this.rbAutoOff = new System.Windows.Forms.RadioButton();
			this.rbNetText = new System.Windows.Forms.RadioButton();
			this.rbAutoTime = new System.Windows.Forms.RadioButton();
			this.cnt0 = new System.Windows.Forms.Label();
			this.cnt1 = new System.Windows.Forms.Label();
			this.btnL0Clear = new System.Windows.Forms.Button();
			this.btnL1Clear = new System.Windows.Forms.Button();
			this.btnLCDTest = new System.Windows.Forms.Button();
			this.lblNetStatus = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rbFont5x7 = new System.Windows.Forms.RadioButton();
			this.rbFont5x10 = new System.Windows.Forms.RadioButton();
			this.btnLCDinit = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.rb1Line = new System.Windows.Forms.RadioButton();
			this.rb2Line = new System.Windows.Forms.RadioButton();
			this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.btnLCDTest2 = new System.Windows.Forms.Button();
			this.btnLeftShift = new System.Windows.Forms.Button();
			this.btnRightShift = new System.Windows.Forms.Button();
			this.chkBoxAutoScroll = new System.Windows.Forms.CheckBox();
			this.timerAutoScroll = new System.Windows.Forms.Timer(this.components);
			this.timerAudioVol = new System.Windows.Forms.Timer(this.components);
			this.labelLeftVol = new System.Windows.Forms.Label();
			this.labelRightVol = new System.Windows.Forms.Label();
			this.labelL = new System.Windows.Forms.Label();
			this.labelR = new System.Windows.Forms.Label();
			this.labelMasterVol = new System.Windows.Forms.Label();
			this.labelM = new System.Windows.Forms.Label();
			this.chkBoxReverse = new System.Windows.Forms.CheckBox();
			this.btnReset = new System.Windows.Forms.Button();
			this.lblScrollCnt = new System.Windows.Forms.Label();
			this.chkIIDX = new System.Windows.Forms.CheckBox();
			this.gboxAutoMode.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnAllClear
			// 
			this.btnAllClear.Location = new System.Drawing.Point(219, 66);
			this.btnAllClear.Name = "btnAllClear";
			this.btnAllClear.Size = new System.Drawing.Size(82, 23);
			this.btnAllClear.TabIndex = 35;
			this.btnAllClear.Text = "LCDクリア";
			this.btnAllClear.UseVisualStyleBackColor = true;
			this.btnAllClear.Click += new System.EventHandler(this.AllClearbtn_Click);
			// 
			// btnAll
			// 
			this.btnAll.Location = new System.Drawing.Point(219, 12);
			this.btnAll.Name = "btnAll";
			this.btnAll.Size = new System.Drawing.Size(82, 50);
			this.btnAll.TabIndex = 27;
			this.btnAll.Text = "一括更新";
			this.btnAll.UseVisualStyleBackColor = true;
			this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
			// 
			// txtAll1
			// 
			this.txtAll1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtAll1.Location = new System.Drawing.Point(12, 37);
			this.txtAll1.MaxLength = 16;
			this.txtAll1.Name = "txtAll1";
			this.txtAll1.Size = new System.Drawing.Size(139, 23);
			this.txtAll1.TabIndex = 26;
			this.txtAll1.Text = "       by yvb776";
			this.txtAll1.TextChanged += new System.EventHandler(this.txtAll1_TextChanged);
			// 
			// txtAll0
			// 
			this.txtAll0.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
			this.txtAll0.Location = new System.Drawing.Point(12, 12);
			this.txtAll0.MaxLength = 16;
			this.txtAll0.Name = "txtAll0";
			this.txtAll0.Size = new System.Drawing.Size(139, 23);
			this.txtAll0.TabIndex = 23;
			this.txtAll0.Text = "LCD.exe     v1.5";
			this.txtAll0.TextChanged += new System.EventHandler(this.txtAll0_TextChanged);
			// 
			// bgAutoTime
			// 
			this.bgAutoTime.WorkerSupportsCancellation = true;
			this.bgAutoTime.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgAutoTime_DoWork);
			this.bgAutoTime.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgAutoTime_RunWorkerCompleted);
			// 
			// gboxAutoMode
			// 
			this.gboxAutoMode.AutoSize = true;
			this.gboxAutoMode.Controls.Add(this.rbAudio);
			this.gboxAutoMode.Controls.Add(this.rbAutoOff);
			this.gboxAutoMode.Controls.Add(this.rbNetText);
			this.gboxAutoMode.Controls.Add(this.rbAutoTime);
			this.gboxAutoMode.Location = new System.Drawing.Point(12, 66);
			this.gboxAutoMode.Name = "gboxAutoMode";
			this.gboxAutoMode.Size = new System.Drawing.Size(139, 118);
			this.gboxAutoMode.TabIndex = 36;
			this.gboxAutoMode.TabStop = false;
			this.gboxAutoMode.Text = "自動更新モード";
			// 
			// rbAudio
			// 
			this.rbAudio.AutoSize = true;
			this.rbAudio.Location = new System.Drawing.Point(6, 62);
			this.rbAudio.Name = "rbAudio";
			this.rbAudio.Size = new System.Drawing.Size(71, 16);
			this.rbAudio.TabIndex = 3;
			this.rbAudio.TabStop = true;
			this.rbAudio.Text = "音量表示";
			this.rbAudio.UseVisualStyleBackColor = true;
			this.rbAudio.CheckedChanged += new System.EventHandler(this.rbAudio_CheckedChanged);
			// 
			// rbAutoOff
			// 
			this.rbAutoOff.AutoSize = true;
			this.rbAutoOff.Checked = true;
			this.rbAutoOff.Location = new System.Drawing.Point(6, 84);
			this.rbAutoOff.Name = "rbAutoOff";
			this.rbAutoOff.Size = new System.Drawing.Size(45, 16);
			this.rbAutoOff.TabIndex = 2;
			this.rbAutoOff.TabStop = true;
			this.rbAutoOff.Text = "OFF";
			this.rbAutoOff.UseVisualStyleBackColor = true;
			this.rbAutoOff.CheckedChanged += new System.EventHandler(this.rbAutoOff_CheckedChanged);
			// 
			// rbNetText
			// 
			this.rbNetText.AutoSize = true;
			this.rbNetText.Location = new System.Drawing.Point(6, 40);
			this.rbNetText.Name = "rbNetText";
			this.rbNetText.Size = new System.Drawing.Size(99, 16);
			this.rbNetText.TabIndex = 1;
			this.rbNetText.Text = "ネットワーク待受";
			this.rbNetText.UseVisualStyleBackColor = true;
			this.rbNetText.CheckedChanged += new System.EventHandler(this.rbNetText_CheckedChanged);
			// 
			// rbAutoTime
			// 
			this.rbAutoTime.AutoSize = true;
			this.rbAutoTime.Location = new System.Drawing.Point(6, 18);
			this.rbAutoTime.Name = "rbAutoTime";
			this.rbAutoTime.Size = new System.Drawing.Size(95, 16);
			this.rbAutoTime.TabIndex = 0;
			this.rbAutoTime.Text = "時計自動更新";
			this.rbAutoTime.UseVisualStyleBackColor = true;
			this.rbAutoTime.CheckedChanged += new System.EventHandler(this.rbAutoTime_CheckedChanged);
			// 
			// cnt0
			// 
			this.cnt0.AutoSize = true;
			this.cnt0.Location = new System.Drawing.Point(157, 19);
			this.cnt0.Name = "cnt0";
			this.cnt0.Size = new System.Drawing.Size(11, 12);
			this.cnt0.TabIndex = 37;
			this.cnt0.Text = "0";
			// 
			// cnt1
			// 
			this.cnt1.AutoSize = true;
			this.cnt1.Location = new System.Drawing.Point(157, 44);
			this.cnt1.Name = "cnt1";
			this.cnt1.Size = new System.Drawing.Size(11, 12);
			this.cnt1.TabIndex = 38;
			this.cnt1.Text = "0";
			// 
			// btnL0Clear
			// 
			this.btnL0Clear.Location = new System.Drawing.Point(174, 16);
			this.btnL0Clear.Name = "btnL0Clear";
			this.btnL0Clear.Size = new System.Drawing.Size(39, 18);
			this.btnL0Clear.TabIndex = 39;
			this.btnL0Clear.Text = "Clr";
			this.btnL0Clear.UseVisualStyleBackColor = true;
			this.btnL0Clear.Click += new System.EventHandler(this.btnL0Clear_Click);
			// 
			// btnL1Clear
			// 
			this.btnL1Clear.Location = new System.Drawing.Point(174, 41);
			this.btnL1Clear.Name = "btnL1Clear";
			this.btnL1Clear.Size = new System.Drawing.Size(39, 18);
			this.btnL1Clear.TabIndex = 40;
			this.btnL1Clear.Text = "Clr";
			this.btnL1Clear.UseVisualStyleBackColor = true;
			this.btnL1Clear.Click += new System.EventHandler(this.btnL1Clear_Click);
			// 
			// btnLCDTest
			// 
			this.btnLCDTest.Location = new System.Drawing.Point(219, 95);
			this.btnLCDTest.Name = "btnLCDTest";
			this.btnLCDTest.Size = new System.Drawing.Size(82, 23);
			this.btnLCDTest.TabIndex = 41;
			this.btnLCDTest.Text = "Font";
			this.btnLCDTest.UseVisualStyleBackColor = true;
			this.btnLCDTest.Click += new System.EventHandler(this.btnLCDTest_Click);
			// 
			// lblNetStatus
			// 
			this.lblNetStatus.AutoSize = true;
			this.lblNetStatus.Location = new System.Drawing.Point(10, 192);
			this.lblNetStatus.Name = "lblNetStatus";
			this.lblNetStatus.Size = new System.Drawing.Size(74, 12);
			this.lblNetStatus.TabIndex = 42;
			this.lblNetStatus.Text = "NetWork OFF";
			// 
			// groupBox1
			// 
			this.groupBox1.AutoSize = true;
			this.groupBox1.Controls.Add(this.rbFont5x7);
			this.groupBox1.Controls.Add(this.rbFont5x10);
			this.groupBox1.Location = new System.Drawing.Point(219, 128);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(82, 76);
			this.groupBox1.TabIndex = 43;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Font";
			// 
			// rbFont5x7
			// 
			this.rbFont5x7.AutoSize = true;
			this.rbFont5x7.Checked = true;
			this.rbFont5x7.Location = new System.Drawing.Point(7, 42);
			this.rbFont5x7.Name = "rbFont5x7";
			this.rbFont5x7.Size = new System.Drawing.Size(41, 16);
			this.rbFont5x7.TabIndex = 1;
			this.rbFont5x7.TabStop = true;
			this.rbFont5x7.Text = "5x7";
			this.rbFont5x7.UseVisualStyleBackColor = true;
			// 
			// rbFont5x10
			// 
			this.rbFont5x10.AutoSize = true;
			this.rbFont5x10.Enabled = false;
			this.rbFont5x10.Location = new System.Drawing.Point(7, 19);
			this.rbFont5x10.Name = "rbFont5x10";
			this.rbFont5x10.Size = new System.Drawing.Size(47, 16);
			this.rbFont5x10.TabIndex = 0;
			this.rbFont5x10.Text = "5x10";
			this.rbFont5x10.UseVisualStyleBackColor = true;
			// 
			// btnLCDinit
			// 
			this.btnLCDinit.Location = new System.Drawing.Point(219, 210);
			this.btnLCDinit.Name = "btnLCDinit";
			this.btnLCDinit.Size = new System.Drawing.Size(82, 23);
			this.btnLCDinit.TabIndex = 44;
			this.btnLCDinit.Text = "LCD初期化";
			this.btnLCDinit.UseVisualStyleBackColor = true;
			this.btnLCDinit.Click += new System.EventHandler(this.btnLCDinit_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.AutoSize = true;
			this.groupBox2.Controls.Add(this.rb1Line);
			this.groupBox2.Controls.Add(this.rb2Line);
			this.groupBox2.Location = new System.Drawing.Point(158, 128);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(55, 76);
			this.groupBox2.TabIndex = 45;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Line";
			// 
			// rb1Line
			// 
			this.rb1Line.AutoSize = true;
			this.rb1Line.Location = new System.Drawing.Point(7, 42);
			this.rb1Line.Name = "rb1Line";
			this.rb1Line.Size = new System.Drawing.Size(41, 16);
			this.rb1Line.TabIndex = 1;
			this.rb1Line.Text = "1行";
			this.rb1Line.UseVisualStyleBackColor = true;
			this.rb1Line.CheckedChanged += new System.EventHandler(this.rb1Line_CheckedChanged);
			// 
			// rb2Line
			// 
			this.rb2Line.AutoSize = true;
			this.rb2Line.Checked = true;
			this.rb2Line.Location = new System.Drawing.Point(7, 19);
			this.rb2Line.Name = "rb2Line";
			this.rb2Line.Size = new System.Drawing.Size(41, 16);
			this.rb2Line.TabIndex = 0;
			this.rb2Line.TabStop = true;
			this.rb2Line.Text = "2行";
			this.rb2Line.UseVisualStyleBackColor = true;
			// 
			// notifyIcon
			// 
			this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
			this.notifyIcon.Text = "USB-IO2.0 LCD Service";
			this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
			// 
			// btnLCDTest2
			// 
			this.btnLCDTest2.Location = new System.Drawing.Point(219, 240);
			this.btnLCDTest2.Name = "btnLCDTest2";
			this.btnLCDTest2.Size = new System.Drawing.Size(82, 23);
			this.btnLCDTest2.TabIndex = 46;
			this.btnLCDTest2.Text = "TEST";
			this.btnLCDTest2.UseVisualStyleBackColor = true;
			this.btnLCDTest2.Click += new System.EventHandler(this.btnLCDTest2_Click);
			// 
			// btnLeftShift
			// 
			this.btnLeftShift.Location = new System.Drawing.Point(12, 210);
			this.btnLeftShift.Name = "btnLeftShift";
			this.btnLeftShift.Size = new System.Drawing.Size(51, 23);
			this.btnLeftShift.TabIndex = 47;
			this.btnLeftShift.Text = "Left";
			this.btnLeftShift.UseVisualStyleBackColor = true;
			this.btnLeftShift.Click += new System.EventHandler(this.btnLeftShift_Click);
			// 
			// btnRightShift
			// 
			this.btnRightShift.Location = new System.Drawing.Point(69, 210);
			this.btnRightShift.Name = "btnRightShift";
			this.btnRightShift.Size = new System.Drawing.Size(51, 23);
			this.btnRightShift.TabIndex = 48;
			this.btnRightShift.Text = "Right";
			this.btnRightShift.UseVisualStyleBackColor = true;
			this.btnRightShift.Click += new System.EventHandler(this.btnRightShift_Click);
			// 
			// chkBoxAutoScroll
			// 
			this.chkBoxAutoScroll.AutoSize = true;
			this.chkBoxAutoScroll.Location = new System.Drawing.Point(12, 239);
			this.chkBoxAutoScroll.Name = "chkBoxAutoScroll";
			this.chkBoxAutoScroll.Size = new System.Drawing.Size(77, 16);
			this.chkBoxAutoScroll.TabIndex = 49;
			this.chkBoxAutoScroll.Text = "AutoScroll";
			this.chkBoxAutoScroll.UseVisualStyleBackColor = true;
			this.chkBoxAutoScroll.CheckedChanged += new System.EventHandler(this.chkBoxAutoScroll_CheckedChanged);
			// 
			// timerAutoScroll
			// 
			this.timerAutoScroll.Tick += new System.EventHandler(this.timerAutoScroll_Tick);
			// 
			// timerAudioVol
			// 
			this.timerAudioVol.Interval = 250;
			this.timerAudioVol.Tick += new System.EventHandler(this.timerAudioVol_Tick);
			// 
			// labelLeftVol
			// 
			this.labelLeftVol.AutoSize = true;
			this.labelLeftVol.Location = new System.Drawing.Point(176, 71);
			this.labelLeftVol.Name = "labelLeftVol";
			this.labelLeftVol.Size = new System.Drawing.Size(17, 12);
			this.labelLeftVol.TabIndex = 50;
			this.labelLeftVol.Text = "-1";
			// 
			// labelRightVol
			// 
			this.labelRightVol.AutoSize = true;
			this.labelRightVol.Location = new System.Drawing.Point(176, 86);
			this.labelRightVol.Name = "labelRightVol";
			this.labelRightVol.Size = new System.Drawing.Size(17, 12);
			this.labelRightVol.TabIndex = 51;
			this.labelRightVol.Text = "-1";
			// 
			// labelL
			// 
			this.labelL.AutoSize = true;
			this.labelL.Location = new System.Drawing.Point(157, 71);
			this.labelL.Name = "labelL";
			this.labelL.Size = new System.Drawing.Size(11, 12);
			this.labelL.TabIndex = 52;
			this.labelL.Text = "L";
			// 
			// labelR
			// 
			this.labelR.AutoSize = true;
			this.labelR.Location = new System.Drawing.Point(157, 86);
			this.labelR.Name = "labelR";
			this.labelR.Size = new System.Drawing.Size(13, 12);
			this.labelR.TabIndex = 53;
			this.labelR.Text = "R";
			// 
			// labelMasterVol
			// 
			this.labelMasterVol.AutoSize = true;
			this.labelMasterVol.Location = new System.Drawing.Point(176, 113);
			this.labelMasterVol.Name = "labelMasterVol";
			this.labelMasterVol.Size = new System.Drawing.Size(17, 12);
			this.labelMasterVol.TabIndex = 54;
			this.labelMasterVol.Text = "-1";
			// 
			// labelM
			// 
			this.labelM.AutoSize = true;
			this.labelM.Location = new System.Drawing.Point(157, 113);
			this.labelM.Name = "labelM";
			this.labelM.Size = new System.Drawing.Size(14, 12);
			this.labelM.TabIndex = 55;
			this.labelM.Text = "M";
			// 
			// chkBoxReverse
			// 
			this.chkBoxReverse.AutoSize = true;
			this.chkBoxReverse.Location = new System.Drawing.Point(95, 239);
			this.chkBoxReverse.Name = "chkBoxReverse";
			this.chkBoxReverse.Size = new System.Drawing.Size(97, 16);
			this.chkBoxReverse.TabIndex = 56;
			this.chkBoxReverse.Text = "Reverse Mode";
			this.chkBoxReverse.UseVisualStyleBackColor = true;
			// 
			// btnReset
			// 
			this.btnReset.Location = new System.Drawing.Point(126, 210);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(51, 23);
			this.btnReset.TabIndex = 57;
			this.btnReset.Text = "Reset";
			this.btnReset.UseVisualStyleBackColor = true;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// lblScrollCnt
			// 
			this.lblScrollCnt.AutoSize = true;
			this.lblScrollCnt.Location = new System.Drawing.Point(183, 215);
			this.lblScrollCnt.Name = "lblScrollCnt";
			this.lblScrollCnt.Size = new System.Drawing.Size(27, 12);
			this.lblScrollCnt.TabIndex = 58;
			this.lblScrollCnt.Text = "N/A";
			// 
			// chkIIDX
			// 
			this.chkIIDX.AutoSize = true;
			this.chkIIDX.Location = new System.Drawing.Point(95, 261);
			this.chkIIDX.Name = "chkIIDX";
			this.chkIIDX.Size = new System.Drawing.Size(76, 16);
			this.chkIIDX.TabIndex = 59;
			this.chkIIDX.Text = "IIDX Mode";
			this.chkIIDX.UseVisualStyleBackColor = true;
			this.chkIIDX.CheckedChanged += new System.EventHandler(this.chkIIDX_CheckedChanged);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(313, 289);
			this.Controls.Add(this.chkIIDX);
			this.Controls.Add(this.lblScrollCnt);
			this.Controls.Add(this.btnReset);
			this.Controls.Add(this.chkBoxReverse);
			this.Controls.Add(this.labelM);
			this.Controls.Add(this.labelMasterVol);
			this.Controls.Add(this.labelR);
			this.Controls.Add(this.labelL);
			this.Controls.Add(this.labelRightVol);
			this.Controls.Add(this.labelLeftVol);
			this.Controls.Add(this.chkBoxAutoScroll);
			this.Controls.Add(this.btnRightShift);
			this.Controls.Add(this.btnLeftShift);
			this.Controls.Add(this.btnLCDTest2);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.btnLCDinit);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.lblNetStatus);
			this.Controls.Add(this.btnLCDTest);
			this.Controls.Add(this.btnL1Clear);
			this.Controls.Add(this.btnL0Clear);
			this.Controls.Add(this.cnt1);
			this.Controls.Add(this.cnt0);
			this.Controls.Add(this.gboxAutoMode);
			this.Controls.Add(this.btnAllClear);
			this.Controls.Add(this.btnAll);
			this.Controls.Add(this.txtAll1);
			this.Controls.Add(this.txtAll0);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "LCD";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ClientSizeChanged += new System.EventHandler(this.Form1_ClientSizeChanged);
			this.gboxAutoMode.ResumeLayout(false);
			this.gboxAutoMode.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btnAllClear;
        internal System.Windows.Forms.Button btnAll;
        internal System.Windows.Forms.TextBox txtAll1;
        internal System.Windows.Forms.TextBox txtAll0;
        internal System.ComponentModel.BackgroundWorker bgAutoTime;
        private System.Windows.Forms.GroupBox gboxAutoMode;
        private System.Windows.Forms.RadioButton rbNetText;
        private System.Windows.Forms.RadioButton rbAutoTime;
        private System.Windows.Forms.RadioButton rbAutoOff;
        private System.Windows.Forms.Label cnt0;
        private System.Windows.Forms.Label cnt1;
        private System.Windows.Forms.Button btnL0Clear;
        private System.Windows.Forms.Button btnL1Clear;
        private System.Windows.Forms.Button btnLCDTest;
        private System.Windows.Forms.Label lblNetStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbFont5x7;
        private System.Windows.Forms.RadioButton rbFont5x10;
        private System.Windows.Forms.Button btnLCDinit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rb1Line;
        private System.Windows.Forms.RadioButton rb2Line;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Button btnLCDTest2;
        private System.Windows.Forms.Button btnLeftShift;
        private System.Windows.Forms.Button btnRightShift;
        private System.Windows.Forms.CheckBox chkBoxAutoScroll;
        private System.Windows.Forms.Timer timerAutoScroll;
        private System.Windows.Forms.Timer timerAudioVol;
        private System.Windows.Forms.Label labelLeftVol;
        private System.Windows.Forms.Label labelRightVol;
        private System.Windows.Forms.Label labelL;
        private System.Windows.Forms.Label labelR;
        private System.Windows.Forms.Label labelMasterVol;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.RadioButton rbAudio;
        private System.Windows.Forms.CheckBox chkBoxReverse;
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.Label lblScrollCnt;
		private System.Windows.Forms.CheckBox chkIIDX;
    }
}

